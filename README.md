# Wharf Self-upgrade #

Create sha1 hash of the PHAR file:
`sha1sum wharf.phar > wharf.version`

Upload `wharf.phar` and `wharf.version`.